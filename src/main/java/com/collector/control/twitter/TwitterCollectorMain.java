package com.collector.control.twitter;

import com.collector.control.twitter.business.TwitterCollectorBusiness;

/**
 * Hello world!
 *
 */
public class TwitterCollectorMain 
{
    public static void main( String[] args )
    {
        TwitterCollectorBusiness business = new TwitterCollectorBusiness();
        business.start();
    }
}

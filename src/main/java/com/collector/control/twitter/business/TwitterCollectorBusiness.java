package com.collector.control.twitter.business;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import com.collector.control.twitter.business.listener.TwitterCollectorListener;

public class TwitterCollectorBusiness {
	
	private static final String TWITTER_PROPERTIES = "resources/twitter.properties";
	private static final String ACCESS_TOKEN = "access.token";
	private static final String ACCESS_TOKEN_SECRET = "access.token.secret";
	private static final String CONSUMER_KEY = "consumer.key";
	private static final String CONSUMER_SECRET = "consumer.secret";
	private TwitterStream stream;
	private Properties properties;

	public TwitterCollectorBusiness() {
		loadPropertyFile();
		loadTwitter();
	}


	public void loadTwitter() {
		TwitterStreamFactory factory = new TwitterStreamFactory(getConfiguration());
		stream = factory.getInstance();
		stream.addListener(new TwitterCollectorListener());
	}


	private Configuration getConfiguration() {
		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setDebugEnabled(true);
		builder.setOAuthConsumerKey(properties.getProperty(ACCESS_TOKEN));
		builder.setOAuthConsumerSecret(properties.getProperty(ACCESS_TOKEN_SECRET));
		builder.setOAuthAccessToken(properties.getProperty(CONSUMER_KEY));
		builder.setOAuthAccessTokenSecret(properties.getProperty(CONSUMER_SECRET));
		
		return builder.build();
	}
	
	public void loadPropertyFile() {
		
		try {
			properties = new Properties();
			properties.load(new FileInputStream(TWITTER_PROPERTIES));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void start(){
		long[] follow = {};
		String[] track = {"dummies"};
		FilterQuery query = new FilterQuery(1, follow, track);
		stream.filter(query);
		
	}
	
	

}

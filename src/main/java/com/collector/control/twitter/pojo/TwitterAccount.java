package com.collector.control.twitter.pojo;

public class TwitterAccount {
	
	private String appID;
	private String appSecret;
	private String consumerID;
	private String consumerSecret;
	
	
	public String getAppID() {
		return appID;
	}


	public void setAppID(String appID) {
		this.appID = appID;
	}


	public String getAppSecret() {
		return appSecret;
	}


	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}


	public String getConsumerID() {
		return consumerID;
	}


	public void setConsumerID(String consumerID) {
		this.consumerID = consumerID;
	}


	public String getConsumerSecret() {
		return consumerSecret;
	}


	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}


	public TwitterAccount() {}
	
	
	

}

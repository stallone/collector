package com.collector.control.model.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.collector.pojo.entity.Tweet;

public class TwitterCollectorDAO {
	
	private static final String TWITTER_COLLECTOR = "twitter_collector";
	private EntityManager entityManager;
	
	public TwitterCollectorDAO(){
		loadEntityManager();
	}

	private void loadEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory(TWITTER_COLLECTOR);
		entityManager = factory.createEntityManager();
	}
	
	public void save(Tweet tweet){
		entityManager.getTransaction().begin();
		entityManager.persist(tweet);
		entityManager.getTransaction().commit();
		
	}
	
	

}

package com.collector.pojo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tweet")
public class Tweet implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long tweetID;
	private String name;
	private String text;
	
	@Id
	@Column(name="tweet_id")
	public Long getTweetID() {
		return tweetID;
	}
	
	public void setTweetID(Long tweetID) {
		this.tweetID = tweetID;
	}
	
	@Column(name="username")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="text")
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

}
